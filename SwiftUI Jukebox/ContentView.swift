//
//  ContentView.swift
//  SwiftUI Jukebox
//
//  Created by Tiago Pereira on 12/12/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  Song.swift
//  SwifwitUI Jukebox
//
//  Created by Tiago Pereira on 12/12/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

class Song {
    let title: String
    let duration: Int
    
    var album: Album?
    
    var artist: Artist {
        return self.album!.artist!
    }
    
    init(title: String, duration: Int) {
        self.title = title
        self.duration = duration
    }
}

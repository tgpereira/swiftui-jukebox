//
//  Jukebox.swift
//  SwifwitUI Jukebox
//
//  Created by Tiago Pereira on 12/12/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation



class JukeboxDatabase {
    
    private var songs: [Song] = []
    
    init() {
        let artist1 = Artist(name: "Kanye West")
        let album1 = Album(name: "JESUS IS KING", coverImageURL: "")
        let song1 = Song(title: "Closed on Sunday", duration: 0)
        let song2 = Song(title: "Everything We Need", duration: 0)
        let album2 = Album(name: "The Life of Pablo", coverImageURL: "")
        let song3 = Song(title: "Ultralight Beam", duration: 0)
        let song4 = Song(title: "Low Lights", duration: 0)
        let song5 = Song(title: "FML", duration: 0)
        
        album1.add(songs: [song1, song2])
        album2.add(songs: [song3, song4, song5])
        
        artist1.add(album: album1)
        artist1.add(album: album2)
        
        let artist2 = Artist(name: "Kendrik Lammar")
        let album3 = Album(name: "DAMN.", coverImageURL: "")
        let song6 = Song(title: "DNA.", duration: 0)
        let song7 = Song(title: "FEEL.", duration: 0)
        let song8 = Song(title: "FEAR.", duration: 0)
        
        album3.add(songs: [song6, song7, song8])
        artist2.add(album: album3)
        
        let artist3 = Artist(name: "Gorillaz")
        let album4 = Album(name: "Demon Days", coverImageURL: "")
        let song9 = Song(title: "El Manana", duration: 0)
        let song10 = Song(title: "Fire Coming Out of the Monkey's Head", duration: 0)
        
        album4.add(songs: [song9, song10])
        artist3.add(album: album4)
        
        let artist4 = Artist(name: "Logic")
        let album5 = Album(name: "Everybody", coverImageURL: "")
        let song11 = Song(title: "Take It Back", duration: 0)
        let song12 = Song(title: "1-800-273-8255", duration: 0)
        
        album5.add(songs: [song11, song12])
        artist4.add(album: album5)
        
        self.songs = [song1, song2, song3, song4, song5, song6, song7, song8, song9, song10, song12]
    }
    
    func allSongs() -> [Song] {
        return self.songs
    }
    
    func allArtists() -> [Artist] {
        var artistsDictionary = [String: Artist]()
        
        for song in self.allSongs() {
            artistsDictionary[song.artist.name] = song.artist
        }
        
        let artistsList = artistsDictionary.values.map { $0 }
        
        return artistsList
    }
    
}

class Jukebox {
    
    // --MARK: Properties
    
    private let database = JukeboxDatabase()
    
    private var currentPlaying: Song?
    
    private var playlist: [Song] = []
    
    // - MARK: Get information
    
    func allSongs() -> [Song] {
        return self.database.allSongs()
    }
    
    func nowPlaying() -> Song? {
        return self.currentPlaying
    }
    
    func nextSongs() -> [Song] {
        return self.playlist
    }
    
    func listOfArtists() -> [Artist] {
        return self.database.allArtists()
    }
    
    // - MARK:
    
    func songs(by artist: Artist) -> [Song] {
        var filteredSongs = [Song]()
        
        filteredSongs = self.allSongs().filter({ $0.artist.name == artist.name })
        
        return filteredSongs
    }
}

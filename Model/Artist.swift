//
//  Artist.swift
//  SwifwitUI Jukebox
//
//  Created by Tiago Pereira on 12/12/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

class Artist {
    let name: String
    
    private var albums: [Album] = []
    
    init(name: String) {
        self.name = name
    }
    
    func add(album: Album) {
        album.artist = self
        self.albums.append(album)
    }
    
    func allAlbums() -> [Album] {
        return self.albums
    }
}

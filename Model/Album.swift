//
//  Album.swift
//  SwifwitUI Jukebox
//
//  Created by Tiago Pereira on 12/12/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

class Album {
    let name: String
    let coverImageURL: String
    
    private var songs: [Song] = []
    
    var artist: Artist?
    
    init(name: String, coverImageURL: String) {
        self.name = name
        self.coverImageURL = coverImageURL
    }
    
    func add(song: Song) {
        song.album = self
        self.songs.append(song)
    }
    
    func add(songs: [Song]) {
        for song in songs {
            song.album = self
            self.songs.append(song)
        }
    }
    
    func allSongs() -> [Song] {
        return self.songs
    }
}
